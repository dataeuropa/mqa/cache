package io.piveau.metrics.cache;

public class Constants {
    private Constants() {}

    public static final String DIMENSION_ACCESSIBILITY = "accessibility";

    public static final String DIMENSION_REUSABLITY = "reusability";

    public static final String DIMENSION_CONTEXTUALITY = "contextuality";

    public static final String DIMENSION_FINDABILITY = "findability";

    public static final String DIMENSION_INTEROPERABILITY = "interoperability";
}
