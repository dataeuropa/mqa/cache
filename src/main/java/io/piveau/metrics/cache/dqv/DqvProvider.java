package io.piveau.metrics.cache.dqv;

import io.piveau.dcatap.TripleStore;
import io.piveau.metrics.cache.persistence.DocumentScope;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;

import java.util.List;

@ProxyGen
public interface DqvProvider {

    String SERVICE_ADDRESS = "metric.cache.dqv";

    static DqvProvider create(TripleStore tripleStore) {
        return new DqvProviderImpl(tripleStore);
    }


    static DqvProvider createProxy(Vertx vertx, String address, DeliveryOptions options) {
        return new DqvProviderVertxEBProxy(vertx, address, options);
    }

    Future<List<String>> listCatalogues();

    // findability
    Future<Double> getKeywordAvailability(String id, DocumentScope documentScope);

    Future<Double> getCategoryAvailability(String id, DocumentScope documentScope);

    Future<Double> getSpatialAvailability(String id, DocumentScope documentScope);

    Future<Double> getTemporalAvailability(String id, DocumentScope documentScope);

    // accessibility
    Future<StatusCodes> getAccessUrlStatusCodes(String id, DocumentScope documentScope);

    Future<Double> getDownloadUrlAvailability(String id, DocumentScope documentScope);

    Future<StatusCodes> getDownloadUrlStatusCodes(String id, DocumentScope documentScope);

    // error status codes
    Future<JsonObject> getDistributionReachabilityDetails(String catalogueId, int offset, int limit, String lang);

    // interoperability
    Future<Double> getFormatAvailability(String id, DocumentScope documentScope);

    Future<Double> getMediaTypeAvailability(String id, DocumentScope documentScope);

    Future<Double> getFormatMediaTypeAlignment(String id, DocumentScope documentScope);

    Future<Double> getFormatMediaTypeNonProprietary(String id, DocumentScope documentScope);

    Future<Double> getFormatMediaTypeMachineReadable(String id, DocumentScope documentScope);

    Future<Double> getDcatApCompliance(String id, DocumentScope documentScope);

    // reusability
    Future<Double> getLicenceAvailability(String id, DocumentScope documentScope);

    Future<Double> getLicenceAlignment(String id, DocumentScope documentScope);

    Future<Double> getAccessRightsAvailability(String id, DocumentScope documentScope);

    Future<Double> getAccessRightsAlignment(String id, DocumentScope documentScope);

    Future<Double> getContactPointAvailability(String id, DocumentScope documentScope);

    Future<Double> getPublisherAvailability(String id, DocumentScope documentScope);

    // contextuality
    Future<Double> getRightsAvailability(String id, DocumentScope documentScope);

    Future<Double> getByteSizeAvailability(String id, DocumentScope documentScope);

    Future<Double> getDateIssuedAvailability(String id, DocumentScope documentScope);

    Future<Double> getDateModifiedAvailability(String id, DocumentScope documentScope);

    // scoring
    Future<Double> getAverageScore(String id, DocumentScope documentScope, String measurementUriRef);

    // catalogue info
    Future<JsonObject> getCatalogueInfo(String catalogueId);

    // catalogue violations
    Future<JsonObject> getCatalogueViolations(String catalogueId, int offset, int limit,String lang);
    Future<JsonObject> getCatalogueViolationsCount(String catalogueId);

    // dataset metrics
    Future<JsonObject> getDatasetMetrics(String datasetId, String lang);
    Future<JsonObject> getDistributionMetricsPerDataset(String datasetId, String lang);

}
