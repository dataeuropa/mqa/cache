# Metrics Cache

The Metrics Cache is designed for storing precomputed statistics derived from DQV (Data Quality Vocabulary) values in a MongoDB database. It acts as a dedicated repository for aggregated scores, particularly those generated from metrics graphs, such as catalog quality scores.

## Table of Contents
1. [Build](#build)
1. [Setup](#setup)
   1. [MongoDB](#mongodb)
1. [Run](#run)
1. [Docker](#docker)
1. [API](#api)
1. [Configuration](#configuration)
   1. [Environment](#environment)
      1. [PIVEAU DCATAP SCHEMA CONFIG](#piveaudcatapschemaconfig)
      1. [PIVEAU TRIPLESTORE CONFIG](#piveautriplestoreconfig)
1. [Logging](#logging)
1. [License](#license)

## Build

Requirements:
 * Git
 * Maven 3
 * Java 17 >=
 * MongoDB

```bash
$ git clone <gitrepouri>
$ cd piveau-metrics-cache
$ mvn package

If you want to skip test cases while running the Maven package command, you can use the following command:

$ mvn package -DskipTests
```

## Setup
### MongoDB

This project utilizes MongoDB as its database. Follow the steps below to set up MongoDB for your development environment.

#### Option 1: Docker Image

1. **Docker Installation:** Make sure you have Docker installed on your machine.

2. **Run MongoDB Container:** Execute the following command in your terminal:

  ```bash
  docker run -d -p 27017:27017 --name mongodb-container mongo
  ```
  This command pulls the official MongoDB Docker image and runs it as a container on port 27017.

#### Option 2: Local Instance

1. **MongoDB Installation:** Install MongoDB on your system.

2. **Run MongoDB Locally:** Navigate to the MongoDB installation directory. (bin folder)

  - Execute the following command:

    ```bash
    .\mongod --port 27017 --dbpath "YOUR MONGO DB DATA PATH"
    ```
     
  Replace the `--dbpath` argument with MongoDB data folder path.
 
## Run

```bash
$ java -jar target/cache.jar
```

## Docker

Build docker image:
```bash
$ docker build -t piveau/metrics-cache .
```

Run docker image:
```bash
$ docker run -it -p 8080:8080 piveau/metrics-cache
```

## API

A formal OpenAPI 3 specification can be found in the `src/main/resources/webroot/openapi.yaml` file.
A visually more appealing version is available at `{url}:{port}` once the application has been started.


## Configuration

### Environment

| Key                           | Description                                                                                                                                                                                             | Default                                                 |
|:------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------|
| `PORT`                        | Port this service will run on                                                                                                                                                                           | 8080                                                    |
| `BASE_URI`                    | Base uri for the Graphs in the Triplestore                                                                                                                                                              | https://piveau.io/                                      |
| `PIVEAU_DCATAP_SCHEMA_CONFIG` | Schema Config for PIVEAU DCATAP                                                                                                                                                                         | `{}` For details, see [here](#piveaudcatapschemaconfig) |
| `H2_PASSWORD`                 | Password for H2 DB                                                                                                                                                                                      | h2pass                                                  |
| `CACHE_APIKEY`                | **Mandatory**: The Apikey to refresh and clear the Cache.                                                                                                                                               |                                                         |
| `CACHE_CORS_DOMAINS`          | Domains from which CORS access should be allowed as Json Array. Example value: `["localhost","example.com"] `                                                                                           |                                                         |
| `MONGODB_SERVER_HOST`         | Hostname this service will try to connect to for the mongo db                                                                                                                                           | localhost                                               |
| `MONGODB_SERVER_PORT`         | Port this service will try to connect to for the mongo db                                                                                                                                               | 27017                                                   |
| `MONGODB_DB_NAME`             | Database name this service will use for communicating with the Mongo DB                                                                                                                                 | metrics                                                 |
| `PIVEAU_TRIPLESTORE_CONFIG`   | Triplestore configuration. The value is a JSON Object, any field that is set in the JSON object for the value of this setting, wil overwrite the default, every field that is missing, will be ignored. | `{}` For details, see [here](#piveautriplestoreconfig)  |
| `PIVEAU_IMPRINT_URL`          | URL to imprint page. Used for OpenAPI GDPR compliance                                                                                                                                                   | `/`                                                     |
| `PIVEAU_PRIVACY_URL`          | URL to privacy policy page. Used for OpenAPI GDPR compliance                                                                                                                                            | `/`                                                     |

#### PIVEAU_DCATAP_SCHEMA_CONFIG

Default values:
```json
{
   "baseUri": "https://piveau.io/",
   "datasetContext": "set/data/",
   "distributionContext": "set/distribution/",
   "recordContext": "set/record/",
   "catalogueContext": "id/catalogue/",
   "metricsContext": "id/metrics/",
   "historyMetricsContext": "id/history_metrics/"
}
```

`baseUri`
: The address

`datasetContext`
: The context path for datasets, appended to the `baseUri`

`distributionContext`
: The context path for distributions, appended to the `baseUri`

`recordContext`
: The context path for records, appended to the `baseUri`

`catalogueContext`
: The context path for catalogues, appended to the `baseUri`

`metricsContext`
: The context path for metrics graphs, appended to the `baseUri`

`historyMetricsContext`
: The context path for metrics history graphs, appended to the `baseUri`

#### PIVEAU_TRIPLESTORE_CONFIG
Default values:
```json
{
   "address":"http://piveau-virtuoso:8890",
   "queryEndpoint": "/sparql", 
   "queryAuthEndpoint":"/sparql-auth", 
   "graphEndpoint":"/sparql-graph-crud", 
   "graphAuthEndpoint":"/sparql-graph-crud-auth",
   "partitionSize":2500, 
   "username":"dba",
   "password":"dba"}
```

`address`
: Base URL of the Virtuoso triplestore.

`queryEndpoint`
: SPARQL query endpoint.

`queryAuthEndpoint`
: Authenticated SPARQL query endpoint.

`graphEndpoint`
: CRUD operations on named graphs.

`graphAuthEndpoint`
: Authenticated CRUD operations on named graphs.

`partitionSize`
: Size of batches for bulk loading data.

`username`
: Username for triplestore authentication.

`password`
: Password for triplestore authentication.

### Logging
See [logback](https://logback.qos.ch/documentation.html) documentation for more details

| Variable                                         | Description                                       | Default Value                           |
|:-------------------------------------------------|:--------------------------------------------------|:----------------------------------------|
| `PIVEAU_CACHE_LOG_APPENDER`                      | Configures the log appender for the cache context | `"STDOUT"`                              |
| `PIVEAU_CACHE_LOG_LEVEL`                         | The log level for the cache context               | `"logs/piveau-pipe.%d{yyyy-MM-dd}.log"` |
| `PIVEAU_LOGSTASH_HOST`                           | The host of the logstash service                  | `"logstash"`                            |
| `PIVEAU_LOGSTASH_PORT`                           | The port the logstash service is running          | `5044`                                  |
| `PIVEAU_LOG_LEVEL`                               | The general log level for the `io.piveau` package | `"INFO"`                                |

## License

[Apache License, Version 2.0](LICENSE.md)
